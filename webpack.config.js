const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const css = './src/style.css';

module.exports = {
  entry: './src/hub.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Hubble Hub",
      template: 'src/index.html'
    })
  ]
};
